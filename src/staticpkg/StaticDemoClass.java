package staticpkg;

public class StaticDemoClass {

	private String make;
	private int instanceNum = 0;
	
	private static int staticNum = 0;

	public StaticDemoClass(String make) {

		this.make = make;
		instanceNum++;
		staticNum++;
	}

	/**
	 * @return the make
	 */
	public String getMake() {
		return make;
	}



	/**
	 * @return the instanceNum
	 */
	public int getInstanceNum() {
		return instanceNum;
	}

	public static int  getstaticNum() {
		return staticNum;
	}

	

}
