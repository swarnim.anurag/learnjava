package pkg1;

public class StringClass {

	public static void main(String[] args) {
		String str = "This is the test string";
		
		System.out.println(str.charAt(0));
		System.out.println(str.length());
		System.out.println(str.concat("This is additional"));
		System.out.println(str.contains("is"));
		System.out.println(str.contains("si"));
		System.out.println(str.startsWith("This"));
		System.out.println(str.startsWith("is"));
		System.out.println("end with string"+str.endsWith("string"));
		System.out.println("end with String"+str.endsWith("String"));
		System.out.println("end with additional"+str.endsWith("additional"));
		

	}

}
