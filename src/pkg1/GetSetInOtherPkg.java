package pkg1;

import oops.ClassedDemo;

public class GetSetInOtherPkg {

	public static void main(String[] args) {
	// create a reference of "ClassdDemo" which is in another(pkg1) pkg
		
		ClassedDemo cd = new ClassedDemo();
		System.out.println(cd.getClass());
		 cd.setColor("Pink");
	        cd.setEngineType("BS4");
	        cd.setModelYear("2020");
	        String col= cd.getColor();
			System.out.println(col);
			System.out.println(cd.getEngineType());
			System.out.println(cd.getModelYear()); 

	}

}
