package oops;

public class ClassedDemo {

	/*
	 * property color model model year engine type Generally property are variable
	 * which is called member variable or class variable or fields
	 */
	private String color;
	private String modelYear;
	private String engineType;
	/*
	 * Behaviour slow fast Generally behaviour are methods
	 */

	public void slowSpeed() {
		System.out.println("Slow speed");

	}

	/**
	 * @return the color
	 */
	public String getColor() {
		return color;
	}

	/**
	 * @param color the color to set
	 */
	public void setColor(String color) {
		this.color = color;
	}

	/**
	 * @return the modelYear
	 */
	public String getModelYear() {
		return modelYear;
	}

	/**
	 * @param modelYear the modelYear to set
	 */
	public void setModelYear(String modelYear) {
		this.modelYear = modelYear;
	}

	/**
	 * @return the engineType
	 */
	public String getEngineType() {
		return engineType;
	}

	/**
	 * @param engineType the engineType to set
	 */
	public void setEngineType(String engineType) {
		this.engineType = engineType;
	}

}
