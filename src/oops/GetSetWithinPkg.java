package oops;

public class GetSetWithinPkg {

	public static void main(String[] args) {
		// create a reference of "ClassdDemo" which is in same pkg
		
		ClassedDemo cd = new ClassedDemo();
        cd.setColor("red");
        cd.setEngineType("BS6");
        cd.setModelYear("2021");
        String col= cd.getColor();
		System.out.println(col);
		System.out.println(cd.getEngineType());
		System.out.println(cd.getModelYear()); 
	}

}
